-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 21, 2017 at 07:53 PM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `prfc`
--

DELIMITER $$
--
-- Functions
--
CREATE DEFINER=`root`@`localhost` FUNCTION `excluiAluno` (`id_excluido` INT) RETURNS VARCHAR(10) CHARSET latin1 BEGIN
declare status_usuario varchar (10);
set status_usuario =  'Excluido';
update usuario set situacao = status_usuario where id_usuario = id_excluido;

RETURN  status_usuario;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `excluiAtividade` (`id_ati` INT) RETURNS VARCHAR(10) CHARSET latin1 BEGIN
declare status_atividade varchar (10);
set status_atividade =  'Excluido';
update atividade set situacao = status_atividade where id_atividade = id_ati ;

RETURN  status_atividade;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `atividade`
--

CREATE TABLE `atividade` (
  `id_atividade` int(11) NOT NULL,
  `desc_atividade` varchar(50) NOT NULL,
  `cod_projeto` int(11) NOT NULL,
  `data_entrega` date NOT NULL,
  `id_status` int(11) DEFAULT NULL,
  `situacao` varchar(40) DEFAULT NULL,
  `data_previsao` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `atividade`
--

INSERT INTO `atividade` (`id_atividade`, `desc_atividade`, `cod_projeto`, `data_entrega`, `id_status`, `situacao`, `data_previsao`) VALUES
(26, 'Desenvolver documentação', 44, '0000-00-00', NULL, 'Iniciado', '2017-10-04'),
(27, 'Desenvolver documentação', 47, '0000-00-00', NULL, 'Iniciado', '2017-10-13'),
(28, 'Desenvolver documentação', 43, '2017-11-01', NULL, 'Iniciado', '2017-10-13'),
(29, 'Desenvolver cadastros', 43, '2017-10-07', NULL, 'Iniciado', '2017-10-03'),
(30, 'Desenvolver cadastros', 44, '2017-10-14', NULL, 'Finalizado', '2017-10-04'),
(31, 'Desenvolver cadastros', 47, '2017-10-21', NULL, 'Finalizado', '2017-10-05'),
(51, 'funcionou', 44, '0000-00-00', NULL, 'Excluido', '2000-12-22'),
(52, 'Desenvoler interfaces', 44, '0000-00-00', NULL, 'Iniciado', '2017-11-18'),
(53, 'Apresentação', 43, '2017-11-10', NULL, 'Finalizado', '2017-11-11'),
(54, 'teste1', 44, '0000-00-00', NULL, 'Excluido', '2017-11-11'),
(55, 'Cadastro de usuario', 51, '0000-00-00', NULL, 'Iniciado', '2017-11-18');

-- --------------------------------------------------------

--
-- Table structure for table `grupo`
--

CREATE TABLE `grupo` (
  `id_grupo` int(11) NOT NULL,
  `id_projeto` int(11) NOT NULL,
  `id_aluno1` int(11) NOT NULL,
  `id_aluno2` int(11) DEFAULT NULL,
  `id_aluno3` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `grupo`
--

INSERT INTO `grupo` (`id_grupo`, `id_projeto`, `id_aluno1`, `id_aluno2`, `id_aluno3`) VALUES
(1, 44, 20, 19, 17),
(2, 47, 16, 14, 13),
(3, 43, 6, 12, 16),
(31, 49, 7, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `projeto`
--

CREATE TABLE `projeto` (
  `id_projeto` int(11) NOT NULL,
  `id_orientador` int(11) NOT NULL,
  `id_coorientador` int(11) NOT NULL,
  `cod_turma` int(11) NOT NULL,
  `nome_projeto` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `projeto`
--

INSERT INTO `projeto` (`id_projeto`, `id_orientador`, `id_coorientador`, `cod_turma`, `nome_projeto`) VALUES
(43, 3, 4, 33, 'Sies'),
(44, 9, 8, 1, 'Agenda escolar'),
(47, 10, 12, 2, 'IPM'),
(49, 10, 12, 1, 'Jornal IFC'),
(50, 9, 12, 2, 'oi'),
(51, 10, 4, 1, 'teste');

-- --------------------------------------------------------

--
-- Table structure for table `registro_atividades`
--

CREATE TABLE `registro_atividades` (
  `id_registro` int(11) NOT NULL,
  `texto` varchar(2000) NOT NULL,
  `data` date NOT NULL,
  `id_aluno` int(11) NOT NULL,
  `cod_atividade` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='	';

--
-- Dumping data for table `registro_atividades`
--

INSERT INTO `registro_atividades` (`id_registro`, `texto`, `data`, `id_aluno`, `cod_atividade`) VALUES
(1, 'Diagrama de classe', '2017-10-05', 17, 26),
(2, 'Diagrama de classe', '2017-10-05', 13, 27),
(3, 'Diagrama de classe', '2017-10-05', 16, 28),
(4, 'Diagrama de caso de uso', '2017-10-28', 6, 28),
(5, 'Diagrama de caso de uso', '2017-11-01', 6, 29),
(6, 'slides', '2017-11-10', 6, 53),
(7, 'Cadastro de professor', '2017-11-17', 6, 29),
(8, 'Cadastro de Aluno', '2017-11-17', 6, 29);

-- --------------------------------------------------------

--
-- Table structure for table `tipouser`
--

CREATE TABLE `tipouser` (
  `id_tipo_user` int(11) NOT NULL,
  `desc_tipo` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tipouser`
--

INSERT INTO `tipouser` (`id_tipo_user`, `desc_tipo`) VALUES
(1, 'admin'),
(2, 'professor'),
(3, 'orientador'),
(4, 'coorientador'),
(5, 'aluno');

-- --------------------------------------------------------

--
-- Table structure for table `turma`
--

CREATE TABLE `turma` (
  `id_turma` int(11) NOT NULL,
  `ano_inicio` varchar(50) NOT NULL,
  `desc_turma` varchar(50) NOT NULL,
  `id_professor` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `turma`
--

INSERT INTO `turma` (`id_turma`, `ano_inicio`, `desc_turma`, `id_professor`) VALUES
(1, '2015', '3info1', 2),
(2, '2016', '2info3', 14),
(3, '2017', '2info2', 17),
(4, '2015', '3info2', 19),
(33, '2016', '3info3', 14),
(35, '2000', '2info1', 2);

-- --------------------------------------------------------

--
-- Table structure for table `usuario`
--

CREATE TABLE `usuario` (
  `id_usuario` int(11) NOT NULL,
  `nome` varchar(50) CHARACTER SET latin1 NOT NULL,
  `email` varchar(50) CHARACTER SET latin1 NOT NULL,
  `senha` varchar(50) CHARACTER SET latin1 NOT NULL,
  `id_tipo` int(11) NOT NULL,
  `cod_turma` int(11) DEFAULT NULL,
  `situacao` varchar(10) CHARACTER SET latin1 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `usuario`
--

INSERT INTO `usuario` (`id_usuario`, `nome`, `email`, `senha`, `id_tipo`, `cod_turma`, `situacao`) VALUES
(1, 'administrador', 'admin@admin.com', '202cb962ac59075b964b07152d234b70', 1, NULL, NULL),
(2, 'Usuario', 'usuario@usuario.com', '202cb962ac59075b964b07152d234b70', 2, NULL, NULL),
(3, 'Orientador', 'orientador@orientador.com', '202cb962ac59075b964b07152d234b70', 3, NULL, NULL),
(4, 'Coorientador', 'coorientador@coorientador.com', '202cb962ac59075b964b07152d234b70', 4, NULL, NULL),
(6, 'Izabelle', 'iza@iza', '202cb962ac59075b964b07152d234b70', 5, 4, 'Ativado'),
(7, 'Rafaela', 'rafa@rafa', '202cb962ac59075b964b07152d234b70', 5, 3, 'Ativado'),
(8, 'Bruno', 'qw@qw', '81dc9bdb52d04dc20036dbd8313ed055', 4, NULL, NULL),
(9, 'Aline', 'aline@aline', '81dc9bdb52d04dc20036dbd8313ed055', 3, NULL, NULL),
(10, 'Bruno', 'bruno@bruno', '81dc9bdb52d04dc20036dbd8313ed055', 3, NULL, NULL),
(12, 'Vanessa', 'va@va', '202cb962ac59075b964b07152d234b70', 4, NULL, NULL),
(13, 'thamires', 'thami@thami', '202cb962ac59075b964b07152d234b70', 5, 2, 'Ativado'),
(14, 'rafael', 'rafael@rafael', '202cb962ac59075b964b07152d234b70', 2, NULL, NULL),
(16, 'gabriel', 'gabriel@gmail.com', '202cb962ac59075b964b07152d234b70', 5, 33, 'Ativado'),
(17, 'speroni', 'speroni@speroni', '202cb962ac59075b964b07152d234b70', 5, 1, 'Excluido'),
(19, 'Edvanderson', 'edvanderson@gmail.com', '202cb962ac59075b964b07152d234b70', 2, NULL, NULL),
(20, 'rafaela', 'rafaelastorti@hotmail.com', '202cb962ac59075b964b07152d234b70', 5, 35, 'Ativado'),
(21, 'Stelio Storti', 'stelio.storti@gmail.com', 'aacf26e0539fb25d0170e84ae09c02f0', 2, NULL, NULL),
(22, 'Julia Nogueira', 'julia@gmail.com', 'aacf26e0539fb25d0170e84ae09c02f0', 5, 4, 'Ativado'),
(23, 'JoÃ£o Silva', 'joao@silva.com', 'aacf26e0539fb25d0170e84ae09c02f0', 5, 4, 'Ativado'),
(25, 'Lucas Martendal', 'lucas@martendal.com', 'aacf26e0539fb25d0170e84ae09c02f0', 5, 4, 'Ativado'),
(28, 'iza', 'iza@gmail.com', 'aacf26e0539fb25d0170e84ae09c02f0', 5, 4, 'Ativado'),
(29, 'Rafael Storti', 'rafaela@storti.com', 'aacf26e0539fb25d0170e84ae09c02f0', 5, 4, 'Ativado'),
(31, 'Elisa Laufer', 'elisa@gmail.com', 'aacf26e0539fb25d0170e84ae09c02f0', 5, 1, 'Ativado'),
(32, 'Ana paula', 'ana@paula.com', 'aacf26e0539fb25d0170e84ae09c02f0', 5, 1, 'Ativado'),
(34, 'Vanessa Tank', 'vanessa@tank.com', 'aacf26e0539fb25d0170e84ae09c02f0', 5, 1, 'Ativado'),
(35, 'giulia', 'giulia@oi.com', '5690dddfa28ae085d23518a035707282', 5, 1, 'Ativado'),
(78, 'joao', 'admin@admin1.com', 'fe008700f25cb28940ca8ed91b23b354', 4, 3, 'Ativado'),
(79, 'joao', 'oi@oii.com', 'fe008700f25cb28940ca8ed91b23b354', 4, 0, 'Ativado'),
(80, 'jefferson', 'jefferson@g.com', 'fe008700f25cb28940ca8ed91b23b354', 5, 1, 'Ativado');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `atividade`
--
ALTER TABLE `atividade`
  ADD PRIMARY KEY (`id_atividade`),
  ADD KEY `cod_projeto` (`cod_projeto`),
  ADD KEY `fkatividadestatus` (`id_status`);

--
-- Indexes for table `grupo`
--
ALTER TABLE `grupo`
  ADD PRIMARY KEY (`id_grupo`),
  ADD KEY `id_projeto` (`id_projeto`),
  ADD KEY `id_aluno1` (`id_aluno1`),
  ADD KEY `id_aluno2` (`id_aluno2`),
  ADD KEY `id_aluno3` (`id_aluno3`);

--
-- Indexes for table `projeto`
--
ALTER TABLE `projeto`
  ADD PRIMARY KEY (`id_projeto`),
  ADD KEY `id_orientador` (`id_orientador`),
  ADD KEY `id_coorientador` (`id_coorientador`),
  ADD KEY `projeto_ibfk_3` (`cod_turma`);

--
-- Indexes for table `registro_atividades`
--
ALTER TABLE `registro_atividades`
  ADD PRIMARY KEY (`id_registro`),
  ADD KEY `cod_atividade` (`cod_atividade`);

--
-- Indexes for table `tipouser`
--
ALTER TABLE `tipouser`
  ADD PRIMARY KEY (`id_tipo_user`);

--
-- Indexes for table `turma`
--
ALTER TABLE `turma`
  ADD PRIMARY KEY (`id_turma`),
  ADD KEY `id_professor` (`id_professor`);

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id_usuario`),
  ADD KEY `id_tipo` (`id_tipo`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `atividade`
--
ALTER TABLE `atividade`
  MODIFY `id_atividade` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;
--
-- AUTO_INCREMENT for table `grupo`
--
ALTER TABLE `grupo`
  MODIFY `id_grupo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `projeto`
--
ALTER TABLE `projeto`
  MODIFY `id_projeto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;
--
-- AUTO_INCREMENT for table `registro_atividades`
--
ALTER TABLE `registro_atividades`
  MODIFY `id_registro` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tipouser`
--
ALTER TABLE `tipouser`
  MODIFY `id_tipo_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `turma`
--
ALTER TABLE `turma`
  MODIFY `id_turma` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=81;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `atividade`
--
ALTER TABLE `atividade`
  ADD CONSTRAINT `atividade_ibfk_1` FOREIGN KEY (`cod_projeto`) REFERENCES `projeto` (`id_projeto`),
  ADD CONSTRAINT `fkatividadestatus` FOREIGN KEY (`id_status`) REFERENCES `status` (`id_status`);

--
-- Constraints for table `grupo`
--
ALTER TABLE `grupo`
  ADD CONSTRAINT `grupo_ibfk_1` FOREIGN KEY (`id_projeto`) REFERENCES `projeto` (`id_projeto`),
  ADD CONSTRAINT `grupo_ibfk_2` FOREIGN KEY (`id_aluno1`) REFERENCES `usuario` (`id_usuario`),
  ADD CONSTRAINT `grupo_ibfk_3` FOREIGN KEY (`id_aluno2`) REFERENCES `usuario` (`id_usuario`),
  ADD CONSTRAINT `grupo_ibfk_4` FOREIGN KEY (`id_aluno3`) REFERENCES `usuario` (`id_usuario`);

--
-- Constraints for table `projeto`
--
ALTER TABLE `projeto`
  ADD CONSTRAINT `projeto_ibfk_1` FOREIGN KEY (`id_orientador`) REFERENCES `usuario` (`id_usuario`),
  ADD CONSTRAINT `projeto_ibfk_2` FOREIGN KEY (`id_coorientador`) REFERENCES `usuario` (`id_usuario`),
  ADD CONSTRAINT `projeto_ibfk_3` FOREIGN KEY (`cod_turma`) REFERENCES `turma` (`id_turma`);

--
-- Constraints for table `registro_atividades`
--
ALTER TABLE `registro_atividades`
  ADD CONSTRAINT `registro_atividades_ibfk_1` FOREIGN KEY (`cod_atividade`) REFERENCES `atividade` (`id_atividade`);

--
-- Constraints for table `turma`
--
ALTER TABLE `turma`
  ADD CONSTRAINT `turma_ibfk_1` FOREIGN KEY (`id_professor`) REFERENCES `usuario` (`id_usuario`);

--
-- Constraints for table `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `usuario_ibfk_1` FOREIGN KEY (`id_tipo`) REFERENCES `tipouser` (`id_tipo_user`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
