
<section class="container">
	<section class="row">
		<section class="form-dialog ">
			<section class="col-md-6 col-md-offset-3 form-box">
				<section class="form-top">

					<h3>Cadastro de projeto</h3>

					<section class="modal-body">
						<form class="signup-form " method="post" action="../../controladores/controlador_cad_projeto.php" enctype="multipart/form-data" role="form">
						        <?php if(isset($_GET['erro'])) :?>

                                    <div class="alert alert-danger alert-dismissible erroentra" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <strong> Atenção!</strong>  <?= @$_GET['erro'] ?>
                                    </div>

                                <?php endif ?>
                                
                                <?php if(isset($_GET['certo'])) :?>

                                    <div class="alert alert-info alert-dismissible erroentra" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <strong> Parabéns!</strong>  <?php echo @$_GET['certo'];?>
                                    </div>
                                <?php endif ?>

							<section class="form-group ">
								<section class="form-input">
									<label>Nome do projeto</label>
									<input class="form-control" name="nome_projeto" placeholder="Ex: Ipm" type="text" >
								</section>
							</section>

							<section class="form-group ">
								<section class="form-input">
								<label>Turma</label>
									<select name="id_turma" class="form-control">
										<option class="form-control">Turma</option>

										<?php
										include_once "../../classe/Turma.php";
										$turma = new Turma();
										$turmas = $turma-> pesquisaTurmas();
										foreach ($turmas as $turma) {
											?>
											<option class="form-control" value="<?=$turma['id_turma'];?>"><?=$turma['desc_turma'];?>
											</option>
										<?php } ?>

									</select>
								</section>
							</section>

								<section class="form-group ">
									<section class="form-input">
									<label>Orientador</label>
										<select name="id_orientador" class="form-control">
											<option class="form-control">Orientador</option>

											<?php
											include_once "../../classe/Usuario.php";
											$orientador = new Usuario ();
											$orientadores = $orientador-> consultaOrientador();
											foreach ($orientadores as $orientador) {
												?>
												<option class="form-control" value="<?=$orientador['id_usuario'];?>"><?=$orientador['nome'];?>
												</option>
												<?php } ?>

											</select>
										</section>
									</section>

									<section class="form-group ">
										<section class="form-input">
										<label>Coorientador</label>
											<select name="id_coorientador" class="form-control">
												<option class="form-control">Coorientador</option>

												<?php
												include_once "../../classe/Usuario.php";
												$coorientador = new Usuario ();
												$coorientadores = $coorientador-> consultaCoorientador();
												foreach ($coorientadores as $coorientador) {
													?>
													<option class="form-control" value="<?=$coorientador['id_usuario'];?>"><?=$coorientador['nome'];?>
													</option>
													<?php } ?>

												</select>
											</section>
										</section>

										<section class="form-group">
											<button type="submit" class="btn btn-block btn-primary btn-lg positivo">Cadastrar
											</button>
										</section>

									</form>
								</section>
							</section>
						</section>
					</section>
				</section>
			</section> 



