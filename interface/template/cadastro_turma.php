<section class="container">
    <section class="row">
        <section class="form-dialog ">
            <section class="col-md-6 col-md-offset-3 form-box">
                <section class="form-top">

                    <h3>Cadastro de turmas</h3>

                    <section class="modal-body">
                        <form class="signup-form " method="post" action="../../controladores/controlador_cad_turma.php" enctype="multipart/form-data" role="form">
                                <?php if(isset($_GET['erro'])) :?>

                                    <div class="alert alert-danger alert-dismissible erroentra" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <strong> Atenção!</strong>  <?= @$_GET['erro'] ?>
                                    </div>

                                <?php endif ?>
                                
                                <?php if(isset($_GET['certo'])) :?>

                                    <div class="alert alert-info alert-dismissible erroentra" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <strong> Parabéns!</strong>  <?php echo @$_GET['certo'];?>
                                    </div>
                                <?php endif ?>

                            <section class="form-group ">
                                <section class="form-input">
                                    <label>Turma</label>
                                    <input class="form-control" name="turma" placeholder="Ex: 3infoi1" type="text">
                                </section>
                            </section>

                            <section class="form-group ">
                                <section class="form-input">
                                    <label>Ano de início</label>
                                    <input autocapitalize="off" autocorrect="off" class="form-control" name="ano_inicio" placeholder="Ex: 2015" type="text">
                                </section>
                            </section>

                            <?php 
                            $id_professor = $_SESSION['login']['id_usuario'];
                            echo ('<input name="id_professor" type="hidden" value="'.$id_professor.'">');
                            ?>

                                <section class="form-group">
                                    <button type="submit" class="btn btn-block btn-primary btn-lg positivo">Cadastrar</button>
                                </section>
                            </form>
                        </section>
                    </section>
                </section>
            </section>
        </section>
    </section>


