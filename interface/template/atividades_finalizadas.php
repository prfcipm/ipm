
<header class="intro dois">
    <article class="intro-body principal">
		<div class="container">
			<h3>Atividades</h3>
			<p>Lista de atividades para conclusão do projeto final de curso: </p>

				<table class="table table-striped">
					<thead>
						<th>Atividade</th>
						<th>Data de conclusão</th>
					</thead>

					<tbody>

					<?php
					include_once '../../classe/Atividade.php';

					$atividade  = new Atividade();
					$id_projeto = $_GET['id_projeto'];

					$consultas = $atividade->listaAtividadesFinalizadas($id_projeto);
					
					foreach ($consultas as $atividade) : ?>	
					<tr>
						<td> <?=$atividade['desc_atividade'] ?></td>
						<td> <?=$atividade['data_entrega'] ?></td>
					</tr>

					<?php endforeach; ?>

				    </tbody>
				</table>	
		</div>
    </article>
</header>
