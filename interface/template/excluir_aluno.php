<?php

include_once "../../classe/Usuario.php";
include_once "../../classe/Turma.php";
include_once '../../classe/Projeto.php';

?>

<section class="container">
  <section class="row">
    <section class="form-dialog ">
      <section class="col-md-6 col-md-offset-3 form-box">
        <section class="form-top">
          <h3>Exclui alunos</h3>
        </section>

         <?php if(isset($_GET['erro'])) :?>

            <div class="alert alert-danger alert-dismissible erroentra" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong> Atenção!</strong>  <?= @$_GET['erro'] ?>
            </div>

        <?php endif ?>
        
        <?php if(isset($_GET['certo'])) :?>

            <div class="alert alert-info alert-dismissible erroentra" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong> Parabéns!</strong>  <?php echo @$_GET['certo'];?>
            </div>
        <?php endif ?>

        <section class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
          <section class="panel panel-default">
            <section class="panel-heading" role="tab" id="headingTwo">
              <h4 class="panel-title">

                <?php

                $turmas = new Turma();
                $lista_turmas = array();
                $turma = $turmas-> pesquisaTurmas();

                foreach ($turma as $turmas) : ?>
                  <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?= $turmas['id_turma'];?>">
                    <h4 class="turma"> Turma <?= $turmas['desc_turma'];?> </h4>
                  </a>
                  <?php 
                    array_push($lista_turmas, $turmas['id_turma']);
                endforeach; ?>

              </h4>
            </section>

          <?php

          foreach ($lista_turmas as $id_turma) : ?>

            <section id="collapse<?=$id_turma; ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
              <section class="panel-body">

                <?php

                $aluno = new Usuario('a','b','c','d');
                $alunos = $aluno->apresentaAlunos($id_turma);

                foreach ($alunos as $aluno) :?>

                  <h4> <?= $aluno['nome'];?>
                  <a href="../../controladores/controlador_exclui_aluno.php?id_usuario=<?=$aluno['id_usuario'];?>"> <span class="glyphicon glyphicon-trash">
                  </a>
                  </h4>
                
                <?php endforeach; ?>

              </section>
            </section>

          <?php endforeach; ?> 

          </section>
        </section>
      </section>
    </section>
  </section>
</section>