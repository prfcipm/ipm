<section class="container">
    <section class="row">
        <section class="form-dialog ">
            <section class="col-md-6 col-md-offset-3 form-box">
                <section class="form-top">

                    <h3>Cadastro de orientador</h3>

                    <section class="modal-body">
                        <form class="signup-form " method="post" action="../../controladores/controlador_cad_orientador.php" enctype="multipart/form-data" role="form">
                            <?php if(isset($_GET['erro'])) :?>

                                    <div class="alert alert-danger alert-dismissible erroentra" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <strong> Atenção!</strong>  <?= @$_GET['erro'] ?>
                                    </div>

                                <?php endif ?>
                                
                                <?php if(isset($_GET['certo'])) :?>

                                    <div class="alert alert-info alert-dismissible erroentra" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <strong> Parabéns!</strong>  <?php echo @$_GET['certo'];?>
                                    </div>
                                <?php endif ?>
                            <section class="form-group ">
                                <section class="form-input">
                                <label>Nome</label>
                                    <input class="form-control" name="nome" placeholder="Ex: João Silva" type="text" >
                                </section>
                            </section>

                            <section class="form-group ">
                                <section class="form-input">
                                <label>Email</label>
                                    <input class="form-control" name="email" placeholder="Ex: joao@silva.com" type="email" >
                                </section>
                            </section>

                            <section class="form-group ">
                                <section class="form-input">
                                <label>Senha</label>
                                    <input class="form-control" name="senha" placeholder="abcd1234" type="password" >
                                </section>
                            </section>

                            <section class="form-group ">
                                <section class="form-input">
                                <label>Confirmar senha</label>
                                    <input class="form-control" name="confirmar_senha" placeholder="abcd1234" type="password" >
                                </section>
                            </section>

                            <input  name="id_tipo" value="3" type="hidden" >

                            <section class="form-group">
                                <button type="submit" class="btn btn-block btn-primary btn-lg positivo">Cadastrar</button>
                            </section>

                        </form>
                    </section>
                </section>
            </section>
        </section>
    </section>
</section>

