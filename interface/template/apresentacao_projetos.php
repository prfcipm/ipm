<section class="container">
  <section class="row">
    <section class="form-dialog ">
      <section class="col-md-6 col-md-offset-3 form-box">
        <section class="form-top">

          <h3>Projetos</h3>

        </section>
        <section class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
          <section class="panel panel-default">
            <section class="panel-heading" role="tab" id="headingTwo">
              <h4 class="panel-title">

                <?php
                include_once '../../classe/Turma.php';
                include_once '../../classe/Grupo.php';

                $lista_turmas = array();
                $id_usuario = $_SESSION['login']['id_usuario'];
                $turmas = new Turma();

                if ($_SESSION['login']['id_tipo']==2){ //professor
                  $turma = $turmas-> apresentaTurmasDoProfessor($id_usuario);
                }

                if (($_SESSION['login']['id_tipo']==3) || ($_SESSION['login']['id_tipo']==4)){ //orientador e coorientador
                  $turma = $turmas-> apresentaTurmasDoOrientador($id_usuario);
                }

                if ($_SESSION['login']['id_tipo']==5){ //aluno
                  $turma = $turmas-> apresentaTurmasDoAluno($id_usuario);
                }

                foreach ($turma as $turmas) {?>
                  <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?= $turmas['id_turma'];?>">
                    <h4 class="turma"> Turma <?= $turmas['desc_turma'];?>
                    </h4>                                  
                  </a>
                  <?php 
                  array_push($lista_turmas, $turmas['id_turma']);
                }?>
              </h4>
              </section>

              <?php
              include_once '../../classe/Projeto.php';
              include_once '../../classe/Turma.php';

              foreach ($lista_turmas as $id_turma) { ?>

                <section id="collapse<?= $id_turma; ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">

                  <section class="panel-body">

                    <?php                              

                    $projetos = new Projeto();
                    
              if($_SESSION['login']['id_tipo']==2){ //professor
                $projeto = $projetos-> apresentaProjeto($id_turma);
              }

              if (($_SESSION['login']['id_tipo']==3) || ($_SESSION['login']['id_tipo']==4)){ //orientador e coorientador
                $projeto = $projetos-> apresentaProjetoOrientado($id_usuario);
              }

              if ($_SESSION['login']['id_tipo']==5){ //aluno
                $projeto = $projetos-> apresentaProjetoAluno($id_usuario);
              }

              foreach ($projeto as $projetos) {?>
                <a href="inicial.php?pos=1&id_projeto=<?= $projetos['id_projeto'];?>&pgs=projeto.php">
                  <h4> <?= $projetos['nome_projeto'];?> </h4>
                </a>                                  

                <?php } ?>
              </section>
              </section>

              <?php } ?>

          </section>
        </section>
      </section>
    </section>
  </section>
</section>
