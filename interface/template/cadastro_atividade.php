
<section class="container">
    <section class="row">
        <section class="form-dialog ">
            <section class="col-md-6 col-md-offset-3 form-box">
                <section class="form-top">
                    <h3>Projeto 

                        <?php
                        include_once('../../classe/Projeto.php');
                        $projeto = new Projeto();
                        $id_projeto = $_GET['id_projeto'];
                        $projetos = $projeto->retornaNomeProjeto($id_projeto);
                        foreach ($projeto as $item) {
                            print($item['nome_projeto']);
                        }
                        ?>
                        
                    </h3>

                    <h3>Cadastro de atividade</h3>
                    <section class="modal-body">
                        <form class="signup-form " method="post" action="../../controladores/controlador_cad_atividade.php"  role="form">
                        
                                <?php if(isset($_GET['erro'])) :?>

                                    <div class="alert alert-danger alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <strong> Atenção!</strong>  <?= @$_GET['erro'] ?>
                                    </div>

                                <?php endif ?>
                                
                                <?php if(isset($_GET['certo'])) :?>

                                    <div class="alert alert-info alert-dismissible " role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <strong> Parabéns!</strong>  <?php echo @$_GET['certo'];?>
                                    </div>
                                <?php endif ?>

                            <section class="form-group ">
                                <section class="form-input">
                                <label>Descrição da atividade</label>
                                    <input class="form-control" name="desc_atividade" placeholder="Ex: Cadastro de usuário" type="text" >
                                </section>
                            </section>

                            <section class="form-group ">
                                <section class="form-input">
                                <?php 
                                echo ('<input type="hidden" name="id_projeto" value="'.$id_projeto.'" class="form-control">
                                    ');
                                ?>
                                    
                                </section>
                            </section>

                            <section class="form-group ">
                                <section class="form-input">
                                <label>Data para entrega</label>
                                    <input class="form-control" name="data_previsao" placeholder="Previsão de entrega" type="date" >
                                </section>
                            </section>


                            <section class="form-group">
                                <button type="submit" class="btn btn-block btn-primary btn-lg positivo">Cadastrar atividade</button>
                            </section>
                        </form>
                    </section>
                </section>
            </section>
        </section>
    </section>
</section>


