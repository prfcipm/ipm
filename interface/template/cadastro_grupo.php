<?php

include_once "../../classe/Usuario.php";
$alunos = Usuario::pesquisaAlunoSemProjetos();

?>

<section class="container">
    <section class="row">
        <section class="form-dialog ">
            <section class="col-md-6 col-md-offset-3 form-box">
                <section class="form-top">
                    <h3>Cadastro de grupo</h3>
                    <section class="modal-body">
                        <form class="signup-form " method="post" action="../../controladores/controlador_cad_grupo.php"  role="form">
                        
                                <?php if(isset($_GET['erro'])) :?>

                                    <div class="alert alert-danger alert-dismissible erroentra" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <strong> Atenção!</strong>  <?= @$_GET['erro'] ?>
                                    </div>

                                <?php endif ?>
                                
                                <?php if(isset($_GET['certo'])) :?>

                                    <div class="alert alert-info alert-dismissible erroentra" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <strong> Parabéns!</strong>  <?php echo @$_GET['certo'];?>
                                    </div>
                                <?php endif ?>

                            <!--ALUNO 1 -->
                            <section class="form-group ">
                                <section class="form-input">
                                <label>Integrante 1</label>
                               
                                    <select name="id_aluno1" class="form-control">
                                        <?php foreach ($alunos as $aluno): ?>
                                            <option class="form-control" value="<?=$aluno['id_usuario'];?>"><?=$aluno['nome'];?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </section>
                            </section>

                            <!--ALUNO 2 -->
                            <section class="form-group ">
                                <section class="form-input">
                                <label>Integrante 2</label>
                                    <select name="id_aluno2" class="form-control">
                                        <option value="">selecione um aluno</option>
                                        <?php foreach ($alunos as $aluno): ?>
                                            <option class="form-control" value="<?=$aluno['id_usuario'];?>"><?=$aluno['nome'];?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </section>
                            </section>

                            <!--ALUNO 3 -->
                            <section class="form-group ">
                                <section class="form-input">
                                <label>Integrante 3</label>
                                    <select name="id_aluno3" class="form-control">
                                        <option value="">selecione um aluno</option>
                                        <?php foreach ($alunos as $aluno): ?>
                                            <option class="form-control" value="<?=$aluno['id_usuario'];?>"><?=$aluno['nome'];?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </section>
                            </section>

                            <section class="form-group ">
                                <section class="form-input">
                                <label>Projeto</label>
                                    <select name="id_projeto" class="form-control">
                                        <?php
                                        include_once "../../classe/Projeto.php";
                                        $projeto = new Projeto();
                                        $projetos = $projeto-> pesquisaProjetos();
                                        foreach ($projetos as $projeto) {
                                            ?>
                                            <option class="form-control" value="<?=$projeto['id_projeto'];?>"><?=$projeto['nome_projeto'];?>
                                            </option>
                                            <?php } ?>
                                        </select>
                                    </section>
                                </section>

                                <section class="form-group">
                                    <button type="submit" class="btn btn-block btn-primary btn-lg positivo">Cadastre-se</button>
                                </section>
                            </form>
                        </section>
                    </section>
                </section>
            </section>
        </section>
    </section>


