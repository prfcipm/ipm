<section class="container">
  <section class="row">
    <section class="form-dialog ">
      <section class="col-md-6 col-md-offset-3 form-box">
        <section class="form-top">

          <h3>Grupos</h3>

        </section>
        <section class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
          <section class="panel panel-default">
            <section class="panel-heading" role="tab" id="headingTwo">
              <h4 class="panel-title"> 

              <?php

                include_once '../../classe/Grupo.php';
                $grupos = new Grupo();
                $grupo = $grupos-> apresentaGrupos();

                foreach ($grupo as $grupos) : ?>
               
                  <h4 class="turma"> Projeto <?= $grupos['nome_projeto'];?> </h4>
                  <h6 class="turma"> Grupo: <?= $grupos['aluno1'];?>,
                   <?= $grupos['aluno2'];?>, <?= $grupos['aluno3'];?> </h6>

                <?php endforeach; ?>

              </h4>
            </section>
            </section>
          </section>
        </section>
      </section>
    </section>
  </section>
</section>
