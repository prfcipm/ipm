<section class="container adm">

      <section class="col-md-3 form-box">
        <section class="form-top">
          <h3>Professores</h3>
        </section>
        <section class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
          <section class="panel panel-default">
          <?php

            include_once '../../classe/Usuario.php';
            $professores = new Usuario('a','b','c','d');
            $professor = $professores-> apresentaProfessor();

            foreach ($professor as $professores) : ?>

            <h4 class="turma"> <?= $professores['nome'];?> </h4>

          <?php endforeach;?>
          </section>
        </section>
      </section>
    

    
      <section class="col-md-3  form-box">
        <section class="form-top">
          <h3>Orientadores</h3>
        </section>
        <section class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
          <section class="panel panel-default">
          <?php
            include_once '../../classe/Usuario.php';
            $professores = new Usuario('a','b','c','d');
            $professor = $professores-> consultaOrientador();

            foreach ($professor as $professores) : ?>

            <h4 class="turma">  <?= $professores['nome'];?> </h4>

          <?php endforeach; ?>
          </section>
        </section>
      </section>

      <section class="col-md-3  form-box">
        <section class="form-top">
          <h3>Coorientadores</h3>
        </section>
        <section class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
          <section class="panel panel-default">

          <?php
            include_once '../../classe/Usuario.php';
            $professores = new Usuario('a','b','c','d');
            $professor = $professores-> consultaCoorientador();

            foreach ($professor as $professores) : ?>

            <h4 class="turma">  <?= $professores['nome'];?> </h4>

          <?php endforeach; ?>

          </section>
        </section>
      </section>

</section>
