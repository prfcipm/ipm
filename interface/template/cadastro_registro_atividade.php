  
<section class="container">
    <section class="row">
        <section class="form-dialog ">
            <section class="col-md-6 col-md-offset-3 form-box">
                <section class="form-top">
                    <h3>Cadastro de sub-atividade</h3>

                    <form class="signup-form " method="post" action="../../controladores/controlador_cad_registro_atividade.php"  role="form">
                                <?php if(isset($_GET['erro'])) :?>

                                    <div class="alert alert-danger alert-dismissible erroentra" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <strong> Atenção!</strong>  <?= @$_GET['erro'] ?>
                                    </div>

                                <?php endif ?>
                                
                                <?php if(isset($_GET['certo'])) :?>

                                    <div class="alert alert-info alert-dismissible erroentra" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <strong> Parabéns!</strong>  <?php echo @$_GET['certo'];?>
                                    </div>
                                <?php endif ?>

                        <input type="hidden" name="id_atividade" value="<?= $_GET['id_atividade'] ?>">
                        <input type="hidden" name="id_projeto" value="<?= $_GET['id_projeto'] ?>">

                         <section class="form-group ">
                                <section class="form-input">
                                    <label>Descrição da Sub-atividade</label>
                                    <input class="form-control" name="texto" placeholder="Ex: Diagrama de classe" type="text" >
                                </section>
                            </section>

                        <section class="form-group">
                            <button type="submit" class="btn btn-block btn-primary btn-lg positivo">Cadastrar
                            </button>
                        </section>
                    </form>


                </section>
            </section>
        </section>
    </section>
</section>


