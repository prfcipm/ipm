<?php
include_once"../../classe/Login.php";

    $logado = new Login();  
    $logado -> isLogado();

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>PRFC</title>

    <!-- FONTS -->
    <link href="../fontes/font_awesome.css" rel="stylesheet">
    <link href="../fontes/lora.css" rel="stylesheet" type="text/css">
    <link href="../fontes/montserrat.css" rel="stylesheet" type="text/css">

    <!-- CSS -->
    <link href="../bootstrap-3.3.6/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="../bootstrap-3.3.6/dist/js/collapse.js" rel="stylesheet">
    <link href="../css/estilo.css" rel="stylesheet"> 

</head>

<body>
    <nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid espaço">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand">IPM</a>
    </div>

<?php
            include_once "../../controladores/controlador.php";
            $tip_user = $_SESSION['login']['id_tipo'];

            $menu = [
            'menu1'=>'inicial.php?pos=1&pgs=projeto.php',
            'menu2'=>'inicial.php?pos=1&pgs=cadastro_atividade.php',
            'menu3'=>'inicial.php?pos=1&pgs=apresentacao_projetos.php',
            'menu4'=>'inicial.php?pos=1&pgs=cadastro_aluno.php',
            'menu5'=>'inicial.php?pos=1&pgs=cadastro_turma.php',
            'menu6'=>'inicial.php?pos=1&pgs=cadastro_projeto.php',
            'menu7'=>'inicial.php?pos=1&pgs=cadastro_grupo.php',
            'menu8'=>'inicial.php?pos=1&pgs=cadastro_professor.php',
            'menu9'=>'inicial.php?pos=1&pgs=cadastro_orientador.php',
            'menu10'=>'inicial.php?pos=1&pgs=cadastro_coorientador.php',
            'menu11'=>'inicial.php?pos=1&pgs=excluir_aluno.php',
            'menu12'=>'inicial.php?pos=1&pgs=apresentacao_grupos.php',
            'menusair'=>'../../controladores/controlador_sair.php'
            ];

            if ($tip_user == 1 ) {
                $template1 = getTemplate('menu_admin.php');
                $templateFinal = parseTemplate( $template1, $menu );
                echo $templateFinal;
              }
            elseif ($tip_user == 2 ) {
                $template1 = getTemplate('menu_professor.php');
                $templateFinal = parseTemplate( $template1, $menu );
                echo $templateFinal;}
            elseif ($tip_user == 3 ) {
                $template1 = getTemplate('menu_orientador.php');
                $templateFinal = parseTemplate( $template1, $menu );
                echo $templateFinal;}
            elseif ($tip_user == 4 ) {
                $template1 = getTemplate('menu_coorientador.php');
                $templateFinal = parseTemplate( $template1, $menu );
                echo $templateFinal;}
            else {
                $template1 = getTemplate('menu_aluno.php');
                $templateFinal = parseTemplate( $template1, $menu );
                echo $templateFinal;
            }

        ?>
</div>
</nav>

 <header class="intro">
    <article class="intro-body principal">

      <?php
        include_once '../../controladores/controlador.php';
        ins_dados(filter_input(INPUT_GET,'pos'), filter_input(INPUT_GET, 'pgs'));
      ?>

    </article>
 </header>

  <footer>
    <section class="row">
      <section> <span class="text"> Rafaela Caroline Storti - 3infoI1 - 2017 </span> </section>
    </section>
  </footer>
</body>

  <script src="../bootstrap-3.3.6-dist/js/jquery.min.js"></script>
  <script src="../bootstrap-3.3.6-dist/js/bootstrap.min.js"></script>

</html>
