<?php

$id_atividade = $_GET['id_atividade'];
$id_projeto = $_GET['id_projeto'];
echo "<form class='signup-form' method='post' enctype='multipart/form-data' action='../../controladores/controlador_edita_atividade.php?id_atividade=".$id_atividade."&id_projeto=".$id_projeto."' role='form'>";

include_once "../../classe/Atividade.php";

$atividade = new Atividade();
$exibicao = $atividade-> apresentaDescAtividade($id_atividade);

?>

<section class="container">
    <section class="row">
        <section class="form-dialog ">
            <section class="col-md-6 col-md-offset-3 form-box">
                <section class="form-top">

                    <h3> Edição de atividade</h3>

                    <section class="modal-body">
                        <form class="signup-form">

                            <?php if(isset($_GET['erro'])) :?>

                                <div class="alert alert-danger alert-dismissible erroentra" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <strong> Atenção!</strong>  <?= @$_GET['erro'] ?>
                                </div>
                            <?php endif ?>
                            
                            <?php if(isset($_GET['certo'])) :?>

                                <div class="alert alert-info alert-dismissible erroentra" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <strong> Parabéns!</strong>  <?php echo @$_GET['certo'];?>
                                </div>
                            <?php endif ?>

                            <section class="form-group ">
                                <section class="form-input">
                                    <label>Descrição da atividade</label>
                                    <input class="form-control" name="edita_atividade" value='<?=$exibicao["desc_atividade"];?>' type="text">
                                </section>
                            </section>

                            <section class="form-group ">
                                <section class="form-input">
                                    <label>Data</label>
                                    <input class="form-control" name="edita_data" value="<?=$exibicao["data_previsao"];?>" type="date">
                                </section>
                            </section>
                            
                            <section class="form-group">
                                <button type="submit" class="btn btn-block btn-primary btn-lg positivo"> Salvar </button>
                            </section>

                        </form>
                    </section>
                </section>
            </section>
        </section>
    </section>
</section> 