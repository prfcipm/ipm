<?php

include_once "Databases.php";

class Grupo {
	
	private $id_projeto;
	private $id_aluno1;
	private $id_aluno2;
	private $id_aluno3;


	public function verificaCadastro($id_aluno){

		$conexao = Databases::getConnection();
		$consulta = $conexao->query("SELECT * FROM grupo WHERE id_aluno1 = $id_aluno or id_aluno2 = $id_aluno or id_aluno3 = $id_aluno");
		$resultado = $consulta->fetch(PDO::FETCH_OBJ);

		if(!$resultado){
			return false; 
		}
		return true;
	}

	public function cadastraGrupo($id_projeto, $id_aluno1, $id_aluno2 = null, $id_aluno3 = null) {

		if($this->verificaCadastro($id_aluno1)){
			echo "O aluno já está cadastrado";
		} else{

		$conexao = Databases::getConnection();
		$consulta  = "INSERT INTO `grupo` (`id_projeto`, `id_aluno1`) VALUES ('$id_projeto','$id_aluno1')";
		
			$conexao->exec($consulta);
		
       		if ($id_aluno2 != null) {
       			$consulta  = "UPDATE `grupo` SET `id_aluno2`=$id_aluno2 WHERE `id_projeto`= $id_projeto";
				$conexao->exec($consulta);
       		}
       		if ($id_aluno3 != null) {
       			$consulta  = "UPDATE `grupo` SET `id_aluno3`=$id_aluno3 WHERE `id_projeto`= $id_projeto";
				$conexao->exec($consulta);
			}
		}
	}

	public function apresentaGrupos(){
		$conexao = Databases:: getConnection();
		$grupo = [];
		$sql = "SELECT g.*, u1.nome as aluno1, u2.nome as aluno2, u3.nome as aluno3, p.* FROM grupo as g, usuario as u1, usuario as u2, usuario as u3, projeto as p WHERE g.id_aluno1=u1.id_usuario AND g.id_aluno2=u2.id_usuario AND g.id_aluno3=u3.id_usuario AND g.id_projeto=p.id_projeto";
		$consulta = $conexao->query($sql);
		$grupo = $consulta->fetchAll(PDO::FETCH_ASSOC);
		return $grupo;
	}
}