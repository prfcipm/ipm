<?php

include_once "Databases.php";

class Usuario {
	
	private $nome;
	private $email;
	private $senha;
	private $id_tipo;
	private $cod_turma;
	private $situacao;
	
	public function __construct($nome, $email,$senha,$id_tipo) {
		$this->nome = $nome;
		$this->email = $email;
		$this->senha = md5($senha);
		$this->id_tipo = $id_tipo;
		$this->cod_turma = NULL;
	} 

	public function setTurma($turma){
		$this->cod_turma = $turma;
	}

	public function confereUsuario(){
		$conexao = Databases::getConnection();
		$consulta = $conexao->query("SELECT * FROM usuario WHERE email = '$this->email'");
		$resultado = $consulta->fetch(PDO::FETCH_OBJ);

		if(isset($resultado) AND !empty($resultado)){
			return true;
		} else {
			return false;
		   
		}
	}

	public function cadastraUsuario(){  
		
		$conexao = Databases::getConnection();
		$consulta  = "INSERT INTO `usuario` ( `nome`, `email`, `senha`, `id_tipo`, `cod_turma`, `situacao`) VALUES ('$this->nome', '$this->email', '$this->senha', '$this->id_tipo', '$this->cod_turma', 'Ativado')";
		var_dump($consulta);
		$conexao->exec($consulta);
		return true;
	}

	public function consultaOrientador(){
		$conexao = Databases:: getConnection();
		$usuario = [];
		$consulta = $conexao->query("SELECT * FROM usuario where id_tipo=3");
		$usuario = $consulta->fetchAll(PDO::FETCH_ASSOC);
		return $usuario;
		}

	public function consultaCoorientador(){
		$conexao = Databases:: getConnection();
		$usuario = [];
		$consulta = $conexao->query("SELECT * FROM usuario where id_tipo=4");
		$usuario = $consulta->fetchAll(PDO::FETCH_ASSOC);
		return $usuario;
		}

	public static function pesquisaAluno(){
		$conexao = Databases::getConnection();
		$usuario = [];
		$consulta = $conexao->query("SELECT * FROM usuario where id_tipo=5 and situacao != 'Excluido'");
		$usuario = $consulta->fetchAll(PDO::FETCH_ASSOC);
		return $usuario;
	}

	public static function pesquisaAlunoSemProjetos(){
		$conexao = Databases::getConnection();
		$usuario = [];
		$consulta = $conexao->query("SELECT * FROM usuario where id_tipo=5 AND 
			id_usuario not in (SELECT id_aluno1 FROM grupo) 
			AND id_usuario not in (SELECT id_aluno2 FROM grupo where id_aluno2 != null) 
			AND id_usuario not in (SELECT id_aluno3 FROM grupo where id_aluno3 != null)");
		$usuario = $consulta->fetchAll(PDO::FETCH_ASSOC);
		return $usuario;
	}

   public function excluiAluno($id_aluno){
		$conexao = Databases::getConnection();
		$consulta = "SELECT excluiAluno($id_aluno)";
		$update = $conexao->query($consulta);
		return $update;
	}

	public function apresentaAlunos($cod_turma){
	 	$conexao = Databases:: getConnection();
	 	$usuario = [];
	 	$consulta = $conexao->query("SELECT nome, id_usuario, id_tipo, cod_turma, situacao FROM usuario where cod_turma = $cod_turma and id_tipo = 5 and situacao != 'Excluido'" );
	 	$usuario = $consulta->fetchAll(PDO::FETCH_ASSOC);
	 	return $usuario;

	 	}

	public function apresentaProfessor(){
	 	$conexao = Databases:: getConnection();
	 	$usuario = [];
	 	$consulta = $conexao->query("SELECT * FROM usuario where id_tipo = 2" );
	 	$usuario = $consulta->fetchAll(PDO::FETCH_ASSOC);
	 	return $usuario;
	 	}
}
