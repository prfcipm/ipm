<?php

include_once "Databases.php";

class Atividade {
	
	private $desc_atividade;
	private $data_previsao;
	private $id_status;
	private $id_projeto;
	private $data_entrega;
	private $situacao;

	public function cadastraAtividade($desc_atividade, $data_previsao, $id_projeto) {  
		$conexao = Databases::getConnection();
		$consulta  = "INSERT INTO `atividade` (`desc_atividade` , `data_previsao`, `cod_projeto`, situacao) VALUES ('$desc_atividade', '$data_previsao', '$id_projeto', 'Iniciado');";
		$conexao->exec($consulta);
		return true;
	}
		
	public function pesquisaAtividade($id_projeto){
  		$conexao = Databases:: getConnection();
		$consulta = $conexao->query("SELECT *, date_format(data_previsao, '%d/%m/%Y') as data FROM atividade where cod_projeto='$id_projeto' and situacao ='Iniciado' ;");
		$atividade = $consulta->fetchAll(PDO::FETCH_ASSOC);
		return $atividade;
	}

	public function pesquisaRegistroAtividade($id_atividade){
  		$conexao = Databases:: getConnection();
		$consulta = $conexao->query("SELECT texto, date_format(data, '%d/%m/%Y') as data, nome
					from registro_atividades, usuario
					where id_aluno = id_usuario
  					and cod_atividade = $id_atividade;");
		$atividade = $consulta->fetchAll(PDO::FETCH_ASSOC);
		return $atividade;
	}

	public function cadastraRegistroAtividade($texto, $data_envio, $id_aluno, $id_atividade){
		$conexao = Databases::getConnection();
		$consulta  = "INSERT INTO `registro_atividades`(`texto`, `data`, `id_aluno`, `cod_atividade`) VALUES ('$texto', '$data_envio', '$id_aluno', '$id_atividade');";
		$conexao->exec($consulta);
		return true;
	}

	public function finalizaAtividade ($id_atividade){
		$conexao = Databases::getConnection();
		$consulta = "UPDATE atividade set situacao = 'Finalizado',
							data_entrega = now()
					 where id_atividade = '$id_atividade'; ";
		$update = $conexao->exec($consulta);
		return $update;
	}

	public function excluiAtividade ($id_atividade){
		$conexao = Databases::getConnection();
		$consulta = "SELECT excluiAtividade($id_atividade)";
		$update = $conexao->query($consulta);
		return $update;
	}

	public function editaAtividade ($id_atividade, $edita_atividade, $edita_data){
		$conexao = Databases::getConnection();
		$consulta = "UPDATE atividade
					 set desc_atividade = '$edita_atividade',
					 	 data_previsao = '$edita_data'
				 	 where id_atividade = '$id_atividade'; ";
		$update = $conexao->exec($consulta);
		return $update;
	}

	public function apresentaDescAtividade($id_atividade){
		$conexao = Databases::getConnection();
		$consulta = "SELECT desc_atividade, data_previsao from atividade where id_atividade=$id_atividade;";
		$resultado = $conexao->query($consulta);
		$retorno = $resultado->fetch(PDO::FETCH_ASSOC);// transforma em array associtivo
		return $retorno;
	}


	public function listaAtividadesFinalizadas($id_projeto){
		$conexao = Databases::getConnection();
		$consulta = "SELECT situacao, desc_atividade, cod_projeto, date_format(data_entrega, '%d/%m/%Y') as data_entrega FROM `atividade` WHERE `situacao`= 'Finalizado' and `cod_projeto`= $id_projeto";
		$resultado = $conexao->query($consulta);
		$retorno = $resultado->fetchAll(PDO::FETCH_ASSOC);

		return $retorno;
	}

}

