<?php

include_once "Databases.php";

class Turma {
	
	public $desc_turma;
	public $ano_inicio;
	public $id_professor;

	
	public function cadastraTurma($desc_turma, $ano_inicio, $id_professor) {        
		$conexao = Databases::getConnection();
		$consulta  = "INSERT INTO `turma`( `desc_turma`, `ano_inicio`, `id_professor`) VALUES ('$desc_turma' , '$ano_inicio' , '$id_professor')";
		$conexao->exec($consulta);
		return true;
	}

	public function pesquisaTurmas(){
		$conexao = Databases:: getConnection();
		$turma = [];
		$pesquisa = "SELECT desc_turma, id_turma FROM turma";
		$consulta = $conexao->query($pesquisa);
		$turma = $consulta->fetchAll(PDO::FETCH_ASSOC);
		return $turma;
	}

	public function apresentaTurmasDoProfessor($id_usuario){
      	$conexao = Databases:: getConnection();
      	$consulta = "SELECT turma.* FROM `turma`, `usuario`,`tipouser` where id_professor = id_usuario and id_tipo = id_tipo_user and id_professor=".$id_usuario;
		$pesquisa = $conexao->query($consulta);
		$professor = $pesquisa->fetchAll(PDO::FETCH_ASSOC);
		return $professor;
	}

	public function apresentaTurmasDoAluno($id_usuario){
      	$conexao = Databases:: getConnection();
      	$consulta = "SELECT turma.* FROM `turma`, `usuario` where turma.id_turma = usuario.cod_turma and id_usuario=".$id_usuario;
		$pesquisa = $conexao->query($consulta);
		$aluno = $pesquisa->fetchAll(PDO::FETCH_ASSOC);
		return $aluno;
	}

	public function apresentaTurmasDoOrientador($id_usuario){
      	$conexao = Databases:: getConnection();
      	$consulta = "SELECT turma.id_turma, turma.ano_inicio, turma.desc_turma,
      				 turma.id_professor, count(turma.id_turma)
					FROM `turma`, `projeto`
					where turma.id_turma = projeto.cod_turma
					  and (id_orientador = ".$id_usuario."
					    or id_coorientador = ".$id_usuario.")
					group by turma.id_turma, turma.ano_inicio, turma.desc_turma,
					turma.id_professor;";
		$pesquisa = $conexao->query($consulta);
		$aluno = $pesquisa->fetchAll(PDO::FETCH_ASSOC);
		return $aluno;
	}


}

	
	