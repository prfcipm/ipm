<?php

	include_once "../classe/Grupo.php";
	include_once "controlador_valida_dados.php";

	$id_projeto = valida($_POST['id_projeto']);
	$id_aluno1 = valida($_POST['id_aluno1']);
	$id_aluno2 = valida($_POST['id_aluno2']);
	$id_aluno3 = valida($_POST['id_aluno3']);
	
	if (!empty($id_aluno1)) {
		if(!empty($id_projeto)) {

		$grupo = new Grupo();
		$resultado = $grupo->cadastraGrupo($id_projeto, $id_aluno1 ,$id_aluno2 ,$id_aluno3);

		if($resultado == "O aluno já está cadastrado") {

			$erros[1] = "Grupo não pode ser cadastrado!";
			header ("location:../interface/template/inicial.php?pos=1&pgs=cadastro_grupo.php&erro=".$erros[1]);
	
		}else{
			
			$certo[1] = "Grupo cadastrado com sucesso!";
			header ("location:../interface/template/inicial.php?pos=1&pgs=cadastro_grupo.php&certo=".$certo[1]);
		}
	}else{
	$erros[1] = "Não existem mais projetos.";
	header ("location:../interface/template/inicial.php?pos=1&pgs=cadastro_grupo.php&erro=".$erros[1]);
	}

	}else{
	$erros[1] = "Preencha o campo Aluno 1!";
	header ("location:../interface/template/inicial.php?pos=1&pgs=cadastro_grupo.php&erro=".$erros[1]);
	
}