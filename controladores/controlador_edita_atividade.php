<?php

include_once "../classe/Atividade.php";
include_once "controlador_valida_dados.php";

$id_atividade = valida($_GET['id_atividade']);
$edita_atividade = valida($_POST['edita_atividade']);
$edita_data = valida($_POST['edita_data']);
$id_projeto = $_GET['id_projeto'];

if (!empty($edita_atividade) && !empty($edita_data)) {

	$atividade = new Atividade();
	$resultado = $atividade -> editaAtividade($id_atividade, $edita_atividade, $edita_data);

	if ($resultado == true) {

		$certo[1] = "Atividade alterada com sucesso!";
		header ("location:../interface/template/inicial.php?pos=1&id_atividade=$id_atividade&pgs=edita_atividade.php&id_projeto=$id_projeto&certo=".$certo[1]);


	}else{

		$erros[1] = "Atividade não pode ser alterada!";
		header ("location:../interface/template/inicial.php?pos=1&id_atividade=$id_atividade&pgs=edita_atividade.php&id_projeto=$id_projeto&erro=".$erros[1]);
	}

	}else{

		$erros[1] = "Preencha todos os campos!";
		header ("location:../interface/template/inicial.php?pos=1&id_atividade=$id_atividade&pgs=edita_atividade.php&id_projeto=$id_projeto&erro=".$erros[1]);
	
	}

?>