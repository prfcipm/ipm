<?php

	include_once "../classe/Projeto.php";
	include_once "controlador_valida_dados.php";

	$id_orientador = valida($_POST['id_orientador']);
	$id_coorientador = valida($_POST['id_coorientador']);
	$id_turma = valida($_POST['id_turma']);
	$nome_projeto = valida($_POST['nome_projeto']);

	if ( !empty($id_orientador) || !empty($id_coorientador) || !empty($id_turma) || !empty($nome_projeto)) {
		
		$projeto = new Projeto();
		$resultado = $projeto->cadastraProjeto($id_orientador, $id_coorientador,$id_turma, $nome_projeto);

		if($resultado == true) {

		$certo[1] = "Projeto cadastrado com sucesso!";
		header ("location:../interface/template/inicial.php?pos=1&pgs=cadastro_projeto.php&certo=".$certo[1]);
	
		}else{

		$erros[1] = "Cadastro não pode ser realizado!";
		header ("location:../interface/template/inicial.php?pos=1&pgs=cadastro_projeto.php&erro=".$erros[1]);
		}
	}else{

		$erros[1] = "Preencha todos os campos";
		header ("location:../interface/template/inicial.php?pos=1&pgs=cadastro_projeto.php&erro=".$erros[1]);
	}