<?php

	include_once "../classe/Usuario.php";
	include_once "controlador_valida_dados.php";

	$nome = valida($_POST['nome']);
	$email = filter_input(INPUT_POST,'email');
	$senha = valida($_POST['senha']);
	$confirmar_senha = valida($_POST['confirmar_senha']);
	$id_tipo = valida($_POST['id_tipo']);
	$id_turma = NULL;
	$situacao = 'Ativado';
	
	
if (!empty($nome) || !empty($email) || !empty($senha) || !empty($confirmar_senha)) {

	if (filter_var($nome, FILTER_VALIDATE_INT)){

		$erros[1] = "O nome deve conter apenas letras";
		header ("location:../interface/template/inicial.php?pos=1&pgs=cadastro_professor.php&erro=".$erros[1]);

	}else{

		if (filter_var($email, FILTER_VALIDATE_EMAIL)) {

			if (preg_match('/[a-zA-Z]/', $senha)) { //Letras maiusculas e minusculas

				if (preg_match('/[0-9]/', $senha)) { //Numeros

					if (strlen($senha) >= 8 ) {

						if ($senha == $confirmar_senha) {

							$usuario = new Usuario($nome, $email, $senha, $id_tipo);
							$pesquisa = $usuario->confereUsuario($email);

							if ($pesquisa == false) { //Se não tem email igual cadastrado

								$resultado = $usuario->cadastraUsuario($nome, $email, $senha, $id_tipo, $cod_turma, $situacao);

								if($resultado == true) {

									$certo[1] = "O usuario foi cadastrado com sucesso!";
									header ("location:../interface/template/inicial.php?pos=1&pgs=cadastro_professor.php&certo=".$certo[1]);
								
								}else{

									echo("Houve um problema no cadastro");}
								
							}else{ // Se já tem email igual cadastrado

								$erros[1] = "O cadastro não pode ser realizado, email já cadastrado";
								header ("location:../interface/template/inicial.php?pos=1&pgs=cadastro_professor.php&erro=".$erros[1]);}

						}else{

							$erros[1] = "Senhas diferentes";
							header ("location:../interface/template/inicial.php?pos=1&pgs=cadastro_professor.php&erro=".$erros[1]);}

					}else{ //pelo menos 8 caracteres
						
						$erros[1] = "Senha deve conter pelo menos 8 caracteres";
						header ("location:../interface/template/inicial.php?pos=1&pgs=cadastro_professor.php&erro=".$erros[1]);}

				}else{ //pelo menos 1 numero
					
					$erros[1] = "Senha deve conter letras e números";
					header ("location:../interface/template/inicial.php?pos=1&pgs=cadastro_professor.php&erro=".$erros[1]);}

				
			}else{

				$erros[1] = "Senha deve conter letras e números";
				header ("location:../interface/template/inicial.php?pos=1&pgs=cadastro_professor.php&erro=".$erros[1]);}
			
		}else{

			$erros[1] = "Formato de email incorreto";
			header ("location:../interface/template/inicial.php?pos=1&pgs=cadastro_professor.php&erro=".$erros[1]);}
		}
}else{

	$erros[1] = "Preencha todos os campos!";
	header ("location:../interface/template/inicial.php?pos=1&pgs=cadastro_professor.php&erro=".$erros[1]);
	
	}