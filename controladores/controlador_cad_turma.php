<?php

	include_once "../classe/Turma.php";
	include_once "controlador_valida_dados.php";

	$desc_turma = valida($_POST['turma']);
	$ano_inicio = valida($_POST['ano_inicio']);
	$id_professor = valida($_POST['id_professor']);
	
	if (!empty($desc_turma) || !empty($ano_inicio) || !empty($id_professor)) {

		if (preg_match('/[a-zA-Z]/', $desc_turma)) { //Letras

				if (preg_match('/[0-9]/', $desc_turma)) { //Numeros


			if (filter_var($ano_inicio, FILTER_VALIDATE_INT)) {

				$turma = new Turma();
				$resultado = $turma->cadastraTurma($desc_turma, $ano_inicio, $id_professor);

				if ($resultado == true) {

				$certo[1] = "Turma cadastrada com sucesso!";
				header ("location:../interface/template/inicial.php?pos=1&pgs=cadastro_turma.php&certo=".$certo[1]);

				}else{

				$erros[1] = "O cadastro não pode ser realizado!";
				header ("location:../interface/template/inicial.php?pos=1&pgs=cadastro_turma.php&erro=".$erros[1]);
				}
			}else{

				$erros[1] = "O ano de ingresso deve conter apenas números!";
				header ("location:../interface/template/inicial.php?pos=1&pgs=cadastro_turma.php&erro=".$erros[1]);

			}
		}else{

			$erros[1] = "O nome da turma deve conter letras e numeros!";
			header ("location:../interface/template/inicial.php?pos=1&pgs=cadastro_turma.php&erro=".$erros[1]);
		}
		}else{

			$erros[1] = "O nome da turma deve conter letras e numeros!";
			header ("location:../interface/template/inicial.php?pos=1&pgs=cadastro_turma.php&erro=".$erros[1]);

		}
	}else{

		$erros[1] = "Preencha todos os campos!";
		header ("location:../interface/template/inicial.php?pos=1&pgs=cadastro_turma.php&erro=".$erros[1]);
	
	}


	

		
		
		