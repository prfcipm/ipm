<?php

	include_once "../classe/Login.php";
	include_once "controlador_valida_dados.php";

	$email = filter_input(INPUT_POST,'email');
	$senha =  valida($_POST['senha']);
	$login = new Login();

	$login->efetuaLogin($email, $senha);
    $teste = $login->isLogado();

    if ($teste == true) {

    	if ($_SESSION['login']['id_tipo'] == 1) { //pagina admin
    		header("location:../interface/template/inicial.php?pos=1&pgs=apresentacao_professores.php");

    	}elseif ($_SESSION['login']['id_tipo'] == 2 || $_SESSION['login']['id_tipo'] == 3 || $_SESSION['login']['id_tipo'] == 4) { // pg professor, orientador e coorientador
    		header("location:../interface/template/inicial.php?pos=1&pgs=apresentacao_projetos.php");

    	}elseif ($_SESSION['login']['id_tipo'] == 5) { // aluno
    		header("location:../interface/template/inicial.php?pos=1&pgs=apresentacao_projetos.php");
    	}
 	
    }else{
	    //$erros[1] = "Email ou senha incorretos!";
        //header ("location:../interface/index.php&erro=".$erros[1]);
        header ("location:../interface/index.php");
	}
    
 ?>