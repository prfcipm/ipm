<?php

	include_once "../classe/Atividade.php";
	include_once "controlador_valida_dados.php";

	$desc_atividade = valida($_POST['desc_atividade']);
	$data_previsao = valida($_POST['data_previsao']);
	$id_projeto = valida($_POST['id_projeto']);

	
if ( !empty($desc_atividade) || !empty($email) || !empty($senha) || !empty($confirmar_senha)) {

	$atividade = new Atividade();
	$resultado = $atividade->cadastraAtividade($desc_atividade, $data_previsao, $id_projeto);

	if($resultado == true) {

		$certo[1] = "Atividade cadastrada com sucesso!";
		header ("location:../interface/template/inicial.php?pos=1&pgs=cadastro_atividade.php&id_projeto=$id_projeto&certo=".$certo[1]);
	
	}else{

		$erros[1] = "Cadastro não pode ser realizado";
		header ("location:../interface/template/inicial.php?pos=1&id_projeto=$id_projeto&pgs=cadastro_atividade.php&erro=".$erros[1]);
	};
	}else{
	$erros[1] = "Preencha todos os campos!";
	header ("location:../interface/template/inicial.php?pos=1&id_projeto=$id_projeto&pgs=cadastro_atividade.php&erro=".$erros[1]);
	
}