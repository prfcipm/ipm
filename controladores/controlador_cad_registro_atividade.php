<?php

	session_start();
	include_once "../classe/Atividade.php";
	include_once "controlador_valida_dados.php";
	
	$texto = valida($_POST['texto']);
	$data_envio = date("Y-m-d");
	$id_aluno = $_SESSION['login']['id_usuario'];
	$id_atividade =  valida($_POST['id_atividade']);
	$id_projeto = valida($_POST['id_projeto']);

	if (!empty($texto) || !empty($data_envio) || !empty($id_aluno) || !empty($id_atividade) || !empty($id_projeto)) {

		$atividade = new Atividade();
		$resultado = $atividade->cadastraRegistroAtividade($texto, $data_envio, $id_aluno, $id_atividade);

		$certo[1] = "Registro de atividade cadastrado com sucesso!";
		header ("location:../interface/template/inicial.php?pos=1&id_atividade=$id_atividade&id_projeto=$id_projeto&pgs=cadastro_registro_atividade.php&certo=".$certo[1]);
	
	}else{

		$erros[1] = "Preencha todos os campos!";
		header ("location:../interface/template/inicial.php?pos=1&pgs=cadastro_registro_atividade.php&erro=".$erros[1]);
	
	}


	